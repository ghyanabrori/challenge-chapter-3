CREATE TABLE pasien
(
    id bigserial PRIMARY KEY,
    nama_pasien VARCHAR(255) NOT NULL,
    keluhan VARCHAR(255) not NULL,
    umur BIGINT NOT NULL
);

CREATE TABLE dokter
(
    id bigserial PRIMARY KEY,
    nama_dokter VARCHAR(255) NOT NULL,
    jam_masuk TIME NOT NULL,
    jam_selesai TIME NOT NULL
);

CREATE TABLE mengakses
(
    id_dokter BIGINT NOT NULL,
    id_obat BIGINT NOT NULL
);

CREATE TABLE diperiksa
(
    id_dokter BIGINT NOT NULL,
    id_pasien BIGINT NOT NULL
);

CREATE TABLE obat
(
    id bigserial PRIMARY KEY,
    nama_obat VARCHAR(255) NOT NULL,
    aturan_pakai VARCHAR(255) NOT NULL
);

INSERT INTO pasien (nama_pasien, keluhan, umur) VALUES
('udin', 'serak tenggorokan', 23),
('samsudin', 'badan panas', 16),
('badarudin', 'bersin-bersin', 40),
('udinaja', 'sakit di perut', 7),
('awaludin', 'pusing', 19),
('akhirudin', 'suara serak', 34);

INSERT INTO dokter (nama_dokter, jam_masuk, jam_selesai) VALUES
('sri', '07:00', '12:00'),
('budi' , '12:00', '17:00'),
('raden', '17:00', '22:00');

INSERT into obat (nama_obat, aturan_pakai) VALUES
('bodrex', '2x1 hari'),
('komix', '3x1 hari'),
('procold', '3x1 hari'),
('paramex', '1x1 hari'),
('diapet', '2x1 hari');

INSERT INTO diperiksa (id_dokter, id_pasien) VALUES
(1,1),
(2,2),
(3,3),
(3,4),
(2,5),
(1,6);

INSERT INTO mengakses (id_dokter, id_obat) VALUES
(1,1),
(1,2),
(1,3),
(1,4),
(1,5),
(2,1),
(2,2),
(2,3),
(2,4),
(2,5),
(3,1),
(3,2),
(3,3),
(3,4),
(3,5);

select * from pasien;

UPDATE pasien SET  nama_pasien = 'udindoang', umur = '32' WHERE id = 1;

